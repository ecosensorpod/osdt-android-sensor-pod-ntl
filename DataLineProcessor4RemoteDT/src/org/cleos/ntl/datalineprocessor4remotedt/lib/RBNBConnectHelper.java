/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * RBNBConnectHelper.java
 * 
 * Create the connection to the DT
 * If something fail, the service will be lock
 * 
 * @author Peter Shin, Gesuri Ramirez
 * @date August 2012
 */

package org.cleos.ntl.datalineprocessor4remotedt.lib;

import org.cleos.android.lib.Utils;
import org.cleos.android.lib.Write2File;
import org.cleos.ntl.datalineprocessor4remotedt.DataLineProcessor_x;

import android.content.Context;
import android.util.Log;

import com.rbnb.sapi.SAPIException;
import com.rbnb.sapi.Source;

public class RBNBConnectHelper {
	Context context;
	String TAG = getClass().getSimpleName();
	String ipp = "localhost:3333";
	String name = "";
	Source dtSrc = null;
	private Write2File log;
	private DataLineProcessor_x dlp_x;

	public void connectToDT(Context c, DataLineProcessor_x dlp_x,
			Write2File log, Source src, String ipp, String name) {
		this.context = c;
		this.dlp_x = dlp_x;
		this.log = log;
		this.dtSrc = src;
		this.ipp = ipp;
		this.name = name;
		new ConnectToDT().start();
	}

	public class ConnectToDT extends Thread {

		@Override
		public void run() {

			try {
				dtSrc.OpenRBNBConnection(ipp, name);
				// if there is no error then unlock DLP service
				Utils.wait(5000);
				Utils.lockService(context, false);
			} catch (SAPIException e) {
				Log.e(TAG, "SAPIException, no connection to " + ipp + " Name: "
						+ name, e);
				// Log.i(TAG,"Error on opening the connection to RBNB server. ");
				log.writelnT("RBNBConnectHelper. Error on opening the connection to RBNB server. "
						+ e.getMessage());
			}
			return;
		}

	}// end class ConnectToDT

}// end class RBNBHelper
