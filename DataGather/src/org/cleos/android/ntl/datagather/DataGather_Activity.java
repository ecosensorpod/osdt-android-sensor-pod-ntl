/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * DataGather_activity.java
 * 
 * Activity to start and stop the DataGather service
 * 
 * @author Gesuri Ramirez
 * @date July 2012
 */

package org.cleos.android.ntl.datagather;

import org.cleos.android.ntl.datagather.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class DataGather_Activity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_gather);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_data_gather, menu);
		return true;
	}

	public void startService(View v) {
		Intent i = new Intent(this, DataGather_Service.class);
		startService(i);
	}

	public void stopService(View v) {
		Intent i = new Intent(this, DataGather_Service.class);
		stopService(i);
	}

}
