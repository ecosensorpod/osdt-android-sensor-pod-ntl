/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
 
 /**
 * Receiver_Lock.java
 * 
 * Broadcast receiver to lock the DT service
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @date July 2012
 */

package org.cleos.dataturbine;

import org.cleos.android.lib.Constants;
import org.cleos.android.lib.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver_Lock extends BroadcastReceiver {
	private String TAG = getClass().getSimpleName();
	private String flagFile = Constants.LOCK_RBNB_SERVICE_FLAG_FILE;

	@Override
	public synchronized void onReceive(Context context, Intent intent) {
		//Log.d(TAG, "Broadcast received!");
		Utils.lockService(context, flagFile, true);
		Log.d(TAG, "The DT service is lock? " + Utils.isLockService(context,flagFile));
	}

}
