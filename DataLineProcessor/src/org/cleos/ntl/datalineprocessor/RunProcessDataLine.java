/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * RunProcessDataLine.java
 * 
 * Thread to start the service and pass the data with the name of the sensor 
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @date August 2012
 */

package org.cleos.ntl.datalineprocessor;

import org.cleos.android.lib.Constants;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RunProcessDataLine extends Thread {
	private String TAG = getClass().getSimpleName();
	private Context context;
	String slcName;
	String dataLine;
	
	public RunProcessDataLine(Context context, String slcName, String dataLine){
		this.context = context;
		this.slcName = slcName;
		this.dataLine = dataLine;
	}
	
	@Override
	public void run() {
		super.run();
		Log.w(TAG,"Got thread!!!");
		
		Intent i=new Intent(this.context, DataLineProcessor_Service.class);
	    i.putExtra(Constants.SLC_NAME, slcName);
	    i.putExtra(Constants.DATALINE, dataLine);
	    this.context.startService(i);
	
	}
}
