/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Receiver_stop.java
 * 
 * Broadcast receiver to stop the DLP service
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @date August 2012
 */

package org.cleos.ntl.datalineprocessor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver_Stop extends BroadcastReceiver {
	private String TAG = getClass().getSimpleName();
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		Log.d(TAG, "Broadcast receiver to StopServiceT.");
		runService();
	}

	private synchronized void runService() {
		Intent serviceIntent = new Intent(this.context,
				DataLineProcessor_Service.class);
		this.context.stopService(serviceIntent);
	}

}
