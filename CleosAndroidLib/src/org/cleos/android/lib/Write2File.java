/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Write2File.java
 * 
 * write string in a file
 * if time option is specified and the file remain open for more than a day, a new string will will be written in a new file
 * 
 * @author Gesuri Ramirez, Peter Shin
 * @date July 2012
 */

package org.cleos.android.lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import android.util.Log;
import android.os.Environment;

public class Write2File {
	private String folderName;
	private String fileName;
	private File root;
	private File gpxfile;
	//private Log log;
	private Calendar createdFileCalendar;
	private TimeHelper th;
	boolean newFileWithTime = false;
	private static String formatString4LogName = "MMdd_HHmmss";

	private static final SimpleDateFormat format = new SimpleDateFormat(
			"MM/dd/yyyy HH:mm:ss-SSS");
	private static final SimpleDateFormat logNameFormat = new SimpleDateFormat(
			formatString4LogName);

	public Write2File(String folderName, String fileName) {
		timeCalledW2F();
		this.folderName = folderName;
		this.fileName = fileName;
		checkCreateFolder();
		gpxfile = new File(root, fileName);
	}// Write2File("FolderName","fileName")

	public Write2File(String folderName, String fileName,
			boolean newFileWithTime) {
		timeCalledW2F();
		this.newFileWithTime = newFileWithTime;
		this.folderName = folderName;
		if (this.newFileWithTime) {
			this.fileName = logNameFormat.format(new Date()) + "_" + fileName;
			// this.createdFileCalendar =
		} else
			this.fileName = fileName;
		checkCreateFolder();
		gpxfile = new File(root, this.fileName);
	}
	
	public Write2File(String folder) {
		timeCalledW2F();
		this.folderName = folder;
		this.fileName = logNameFormat.format(new Date()) + "_log.txt";
		checkCreateFolder();
		gpxfile = new File(root, fileName);
	}

	public Write2File() {
		timeCalledW2F();
		folderName = "Write2File";
		fileName = "Log.txt";
		checkCreateFolder();
		gpxfile = new File(root, fileName);
	}

	private void timeCalledW2F() {
		this.th = new TimeHelper();
		this.createdFileCalendar = th.now();
	}

	public void setNewFileWithTime(boolean boo) {
		this.newFileWithTime = boo;
	}

	private boolean checkCreateFolder() {
		root = new File(Environment.getExternalStorageDirectory(), folderName);
		if (!root.exists()) {
			root.mkdirs();
			return true;
		} else
			return false;
	}

	public boolean writeln(String str) {
		try {
			BufferedWriter bW = new BufferedWriter(
					new FileWriter(gpxfile, true));
			bW.write(str);
			bW.newLine();
			bW.flush();
			bW.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean writelnT(String str) {
		if (this.newFileWithTime) {
			int timeField = Calendar.DAY_OF_YEAR;
			if (this.createdFileCalendar.get(timeField) != th.now().get(
					timeField)) {
				updateSetup();
			}
		}
		return writeln(format.format(new Date()) + "--> " + str);
	}
	
	private void updateSetup(){
		int startIndex = Write2File.formatString4LogName.length()+1;
		String justFileName = this.fileName.substring(startIndex);
		timeCalledW2F();
		this.fileName = logNameFormat.format(new Date()) + "_" + justFileName;
		checkCreateFolder();
		this.gpxfile = new File(this.root, this.fileName);
	}


} // end class Write2File
