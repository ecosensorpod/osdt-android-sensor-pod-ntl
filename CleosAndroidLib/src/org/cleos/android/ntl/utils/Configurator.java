/*
 Copyright 2012 University of California, San Diego

 Licensed under the Apache License, Version 1.1 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-1.1

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Configurator.java
 * 
 * Create the command and commandList for each sensor
 * Sensors: Sonde, Templine, Vaisela Weather Station
 * Onboard sensor (analog sensor attached to the IOIO board): temperature, humidity, and voltage
 * 
 * @author Peter Shin, Gesuri Ramirez
 * @date August 2012
 */

package org.cleos.android.ntl.utils;

import java.util.Calendar;
import java.util.LinkedList;

import org.cleos.android.lib.Interval;
import org.cleos.android.lib.TimeHelper;

import org.cleos.android.ntl.utils.Command;
import org.cleos.android.ntl.utils.CommandList;


public class Configurator {
	private TimeHelper th = new TimeHelper();
	private char endLine = 13;
	//private String remoteDtAddress = "192.168.1.4:3333";
	private String remoteDtAddress = "184.169.156.148:3333";// "192.168.1.48:3333";
															// //"184.169.156.148:3333";
	private String localDtAddress = "localhost:3333";

	// sonde
	public CommandList createSondeCmdList(String name) {
		// 0+23.83+2.57+0.18+0+0.0+0.00+12.5
		String[] chNames = { "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8" };

		String[] dTypes = { "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64" };

		String[] units = { "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius", "Celsius" };

		String[] MIMEs = { "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream" };

		LinkedList<Command> sondeCmdList = new LinkedList<Command>();
		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval tCmd1 = new Interval(0, 0, 1, 0); //read every 1 minute
		Interval tCmd2 = new Interval(1, 0, 0, 0); //clean every 12 hours

		// read command
		Command tempCmd = new Command("0R0!", "regularExpression", "", 5000, 3,
				1, tCmd1);
		tempCmd.setDtSrcName("SondeSrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter("+");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		// clean command
		Command tempCleanCmd = new Command("0XC!", "regularExpression", "", 1,
				1, 1, tCmd2);

		sondeCmdList.add(tempCmd);
		sondeCmdList.add(tempCleanCmd);

		CommandList cmdList = new CommandList(name, sondeCmdList,
				startDateTime, endDateTime);

		return cmdList;
	}

	public CommandList createTemplineCmdList(String name) {
		String[] chNames = { "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8",
				"t9", "t10", "t11", "t12", "t13", "t14", "t15", "t16", "t17",
				"t18", "t19", "t20", "t21", "t22", "t23", "t24", "t25", "t26",
				"t27" };

		String[] dTypes = { "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64" };

		String[] units = { "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius", "Celsius", "Celsius",
				"Celsius", "Celsius", "Celsius" };

		String[] MIMEs = { "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream" };

		LinkedList<Command> tempCommandList = new LinkedList<Command>();

		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval interval = new Interval(0, 0, 1, 0);

		Command tempCmd = new Command("", "regularExpression", "" + endLine,
				5000, 3, 1, interval);
		tempCmd.setDtSrcName("TemplineSrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter(" ");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		tempCommandList.add(tempCmd);

		CommandList cmdList = new CommandList(name, tempCommandList,
				startDateTime, endDateTime);

		return cmdList;
	}

	public CommandList createTempCmdList(String name) {

		String[] chNames = { "temperature" };

		String[] dTypes = { "float64" };

		String[] units = { "Celsius" };

		String[] MIMEs = { "application/octet-stream" };

		LinkedList<Command> tempCommandList = new LinkedList<Command>();

		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval interval = new Interval(0, 0, 1, 0); // days, hours/24,
														// min/60, sec/60

		Command tempCmd = new Command("@T", "regularExpression", "", 5000, 3,
				1, interval);
		tempCmd.setDtSrcName("BoardTempSrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter("");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		tempCommandList.add(tempCmd);

		CommandList cmdList = new CommandList(name, tempCommandList,
				startDateTime, endDateTime);

		return cmdList;
	}

	public CommandList createHumiCmdList(String name) {
		String[] chNames = { "humidity" };

		String[] dTypes = { "float64" };

		String[] units = { "Percent" };

		String[] MIMEs = { "application/octet-stream" };

		LinkedList<Command> tempCommandList = new LinkedList<Command>();

		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval interval = new Interval(0, 0, 1, 0); // days, hours/24,
		// min/60, sec/60

		Command tempCmd = new Command("@H", "regularExpression", "", 5000, 3,
				1, interval);
		tempCmd.setDtSrcName("BoardHumiditySrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter("");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		tempCommandList.add(tempCmd);

		CommandList cmdList = new CommandList(name, tempCommandList,
				startDateTime, endDateTime);
		return cmdList;
	}

	public CommandList createVoltCmdList(String name) {
		String[] chNames = { "voltage" };

		String[] dTypes = { "float64" };

		String[] units = { "volts" };

		String[] MIMEs = { "application/octet-stream" };

		LinkedList<Command> tempCommandList = new LinkedList<Command>();

		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval interval = new Interval(0, 0, 1, 0); // days, hours/24,
														// min/60, sec/60

		Command tempCmd = new Command("@V", "regularExpression", "", 5000, 3,
				1, interval);
		tempCmd.setDtSrcName("BoardVoltageSrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter("");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		tempCommandList.add(tempCmd);

		CommandList cmdList = new CommandList(name, tempCommandList,
				startDateTime, endDateTime);
		return cmdList;
	}

	public CommandList createVWSCmdList(String name) {
		String[] chNames = { "C0", "C1", "WindDirAvg", "WindDirMax",
				"WindSpeMin", "WindSpeAvg", "WindSpeMax", "AirTemp", "RelHumi",
				"AirPress", "RainAcc", "RainDur", "RainInt", "HailAcc",
				"HailDur", "HailInt", "RainPeakInt", "SupVol", "RefVol" };

		String[] dTypes = { "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64",
				"float64", "float64", "float64", "float64", "float64" };

		String[] units = { "none", "none", "Deg", "Deg", "m/s", "m/s", "m/s",
				"Celsius", "$RH", "hPa", "mm", "Seconds", "mm/h", "hits/cm^2",
				"Seconds", "hits/cm^2h", "mm/h", "Volts", "Volts" };

		String[] MIMEs = { "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream",
				"application/octet-stream", "application/octet-stream" };

		LinkedList<Command> tempCommandList = new LinkedList<Command>();

		Calendar startDateTime = th.now();
		Calendar endDateTime = null;

		Interval interval = new Interval(0, 0, 1, 0);// days, hours/24,
		// min/60, sec/60

		char CR = 13;
		char LF = 10;
		Command tempCmd = new Command("0R0", "regularExpression", "" + CR + LF,
				5000, 3, 1, interval);
		tempCmd.setDtSrcName("VWSSrc");
		tempCmd.setDtAddress(localDtAddress);
		tempCmd.setRemoteDtAddress(remoteDtAddress);
		tempCmd.setDelimiter("[,=CDHMPRSTUVacdimnprsx]+");
		tempCmd.setChNames(chNames);
		tempCmd.setDTypes(dTypes);
		tempCmd.setUnits(units);
		tempCmd.setMIMEs(MIMEs);

		tempCommandList.add(tempCmd);

		CommandList cmdList = new CommandList(name, tempCommandList,
				startDateTime, endDateTime);

		return cmdList;
	}

}